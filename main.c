/*
 * HelloWorld.c
 *
 * Created: 9/7/2016 7:11:33 PM
 * Author : Suyoung
 */ 

#include <avr/interrupt.h>
#include "uart0.h"

int main(void)
{
	sei();
	UART0_initialize();
	unsigned char adsf = 0;
	UART0_rx_flush(&adsf);
	UART0_tx_request_retransmission();
	UART0_tx_put_char('a');
}