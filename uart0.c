/*
 * CFile1.c
 *
 * Created: 9/9/2016 10:21:25 PM
 *  Author: Suyoung
 */ 

#include "uart0.h"
#include <avr/interrupt.h>
#include <ctype.h>
#include <stdlib.h>

unsigned char UART0_rx_buffer[UART0_RX_BUFFER_SIZE];
unsigned char UART0_rx_size;
unsigned char UART0_rx_finished;
unsigned char UART0_rx_checksum_index;
unsigned char UART0_rx_error;
unsigned char UART0_tx_buffer[UART0_TX_BUFFER_SIZE];

unsigned char UART0_tx_head;
unsigned char UART0_tx_size;

unsigned char UART0_last_line;

unsigned char UART0_length;
unsigned char UART0_errno;

static void UART0_tx_put_char_internal(unsigned char data);
static void UART0_tx_put_int(unsigned int line);

void UART0_initialize() {
	UART0_rx_size = 0;
	UART0_rx_checksum_index = 0;
	UART0_rx_error = 0;
	UART0_rx_finished = 0;
	UART0_tx_head = 0;
	UART0_tx_size = 0;
	UART0_last_line = 0;
	UART0_length = 0;
	UART0_errno = 0;
}

static void UART0_rx_reset() {
	cli();
	UART0_rx_size = 0;
	UART0_rx_checksum_index = 0;
	UART0_rx_error = 0;
	UART0_rx_finished = 0;
	sei();
}

//Returns length of the read data.
char UART0_rx_flush(unsigned char* buffer) {
	//Block until RX is finished.
	while(!UART0_rx_finished);
	//Check for error.
	if(UART0_rx_error) {
		UART0_tx_put_char(NCK);
		//Frame error.
		if(UART0_rx_error & (1 << FE0)) {
			UART0_errno = UART_ERROR_FRAME_ERROR;
		//Data overrun error.		
		} else if(UART0_rx_error & DOR0) {
			UART0_errno = UART_ERROR_DATA_OVERRUN_ERROR;
		//Parity error.
		} else {
			UART0_errno = UART_ERROR_PARITY_ERROR;
		}
		UART0_tx_put_char(UART0_errno);
		//Send last processed line.
		UART0_tx_put_int(UART0_last_line);
		UART0_rx_reset();
		return 0;
	}
	//Calculate checksum if possible.
	if(UART0_rx_checksum_index) {
		unsigned char checksum = atoi((const char*)&UART0_rx_buffer[UART0_rx_checksum_index]) && 0xFF;
		unsigned char calc = 0;
		for(unsigned char i = 0; i < UART0_rx_checksum_index - 1; ++i) {
			calc ^= UART0_rx_buffer[i];
		}
		if(calc != checksum) {
			UART0_tx_put_char(NCK);
			UART0_errno = UART_ERROR_INVALID_CHECKSUM;
			UART0_tx_put_char(UART_ERROR_INVALID_CHECKSUM);
			//Send last processed line.
			UART0_tx_put_int(UART0_last_line);
			UART0_rx_reset();
			return 0;
		}
	}
	UART0_errno = UART_ERROR_NONE;
	//Store read size for return value.
	unsigned char size = UART0_rx_size;
	//Calculate line number.
	unsigned int line = atoi((const char*)&UART0_rx_buffer[1]);
	//Send ACK.
	UART0_tx_put_char(ACK);
	//Send processed line.
	UART0_tx_put_int(line);
	//Re-initialize variables.
	UART0_rx_reset();
	//Update last processed line.
	UART0_last_line = line;
	return size;
}

void UART0_tx_request_retransmission() {
	//Send last processed line.
	UART0_tx_request_retransmission_line(UART0_last_line);
}

void UART0_tx_request_retransmission_line(unsigned int line) {
	UART0_tx_put_char(ENQ);
	UART0_tx_put_int(line);
}

void UART0_tx_put_char(unsigned char data) {
	UART0_tx_put_char_internal(data);
	//Enable Data Register Empty interrupt.
	UCSR0B |= (1 << UDRIE0);
}

static void UART0_tx_put_int(unsigned int line) {
	UART0_tx_put_char_internal(line && 0xFF);
	UART0_tx_put_char_internal((line >> 8) && 0xFF);
	//Enable Data Register Empty interrupt.
	UCSR0B |= (1 << UDRIE0);
}

void UART0_tx_put_char_internal(unsigned char data) {
	while(UART0_tx_size >= UART0_TX_BUFFER_SIZE);
	UART0_tx_buffer[(UART0_tx_head + UART0_tx_size) % UART0_TX_BUFFER_SIZE] = data;
	++UART0_tx_size;
}

ISR(USART0_UDRE_vect) {
	if(!UART0_tx_size) {
		//Disable Data Register Empty interrupt.
		UCSR0B &= ~(1 << UDRIE0);
	} else {
		UDR0 = UART0_tx_buffer[UART0_tx_head++];
		if(UART0_tx_head >= UART0_TX_BUFFER_SIZE) {
			UART0_tx_head = 0;
		}
		--UART0_tx_size;
	}
}

ISR(USART0_RX_vect) {
	//If the previous message wasn't handled yet, ignore.
	if(UART0_rx_finished) {
		return;
	}
	//Check for error.
	UART0_rx_error |= UCSR0A & ((1 << FE0) | (1 << DOR0) | (1 << UPE0));
	//Read value.
	unsigned char read = UDR0;
	//If newline or comment character is received, mark the end of message.
	//Use bitwise OR to improve speed.
	if(('\n' == read) | (';' == read)) {
		UART0_rx_finished = 1;
		return;
	}
	//Handle checksum.
	if('*' == read) {
		UART0_rx_checksum_index = UART0_rx_size + 1;
	}
	//Buffer overflow
	if(UART0_rx_size >= UART0_RX_BUFFER_SIZE) {
		return;
	}
	UART0_rx_buffer[UART0_rx_size++] = read;
}
