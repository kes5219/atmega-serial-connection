/*
 * uart0.h
 *
 * Created: 9/10/2016 1:52:35 AM
 *  Author: Suyoung
 */ 

#ifndef UART0_H_
#define UART0_H_

#define UART0_RX_BUFFER_SIZE 128
#define UART0_TX_BUFFER_SIZE 128

#define ACK 0x06
#define NCK 0x15
#define ENQ 0x05

//Error codes
#define UART_ERROR_NONE 0
#define UART_ERROR_INVALID_CHECKSUM 1
#define UART_ERROR_FRAME_ERROR 2
#define UART_ERROR_DATA_OVERRUN_ERROR 3
#define UART_ERROR_PARITY_ERROR 4
#define UART_ERROR_INVALID_FORMAT 5

extern unsigned char UART0_rx_buffer[UART0_RX_BUFFER_SIZE];
extern unsigned char UART0_rx_size;
extern unsigned char UART0_rx_finished;
extern unsigned char UART0_rx_checksum_index;
extern unsigned char UART0_rx_error;
extern unsigned char UART0_tx_buffer[UART0_TX_BUFFER_SIZE];

extern unsigned char UART0_tx_head;
extern unsigned char UART0_tx_size;

extern unsigned char UART0_last_line;

extern unsigned char UART0_length;
extern unsigned char UART0_errno;

extern void UART0_initialize();
extern char UART0_rx_flush(unsigned char* buffer);
extern void UART0_tx_request_retransmission();
extern void UART0_tx_request_retransmission_line(unsigned int line);
extern void UART0_tx_put_char(unsigned char data);

#endif /* UART0_H_ */